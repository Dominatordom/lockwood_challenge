Dear Dominic,

We kindly ask that you complete the below assignment as part of your application.

Imagine you've been asked to design and implement a timeline of events happening in a company.

You decided to write an API that will allow the register of events in order to display them later. Examples of events are: Deployment of the fresh release of the game your company makes.
Changes of configuration for upcoming event in the game.
Game outage, affecting one or more mobile platforms.
Some other events of your choice (please think what other events can occur in the system).

Task: 
1. Implement API to store and retrieve events using framework of your choice.
2. API should allow to filter events at least by event type.
3. Implement a simple web page to display the events or CLI.
4. If possible - web page or cli should display events in real time (receive updates).

Only python3.

Please reply to confirm your availability to complete the assignment. If you have any further questions do not hesitate to contact me.

Sincerely,

Gerard Dowling 
