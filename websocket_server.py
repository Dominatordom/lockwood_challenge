#!/usr/bin/python3
# -*- coding: utf-8 -*-

import asyncio
import websockets
import json
import os
import config

DB = {}


async def sendtoall(path, message):
    for k in DB.keys():
        if path.startswith(k):
            for con in DB[k]:
                if not con.closed:
                    try:
                        await con.send(message)
                    except:
                        pass


async def newconnection(websocket, path):
    """ messages from clients are discarded, messages sent to /send will be processed and sent to a path depending on the source
        example: /data/datasource_13_values will get any data inserted on child tables
        note: data is a json dump of a dict where the keys are the column names
    """

    print("new connection {0} to {1}".format(str(websocket.remote_address), path))
    DB.setdefault(path, []).append(websocket)
    DB[path] = [x for x in DB[path] if not x.closed]
    sender = path == "/send"

    async for message in websocket:
        try:
            await websocket.send("received")
            if sender:
                # only do something with message if is data sender
                try:
                    if message != "ping":
                        await sendtoall("/receive", "new event")
                except Exception as e:
                    print("error sending data - {}".format(repr(e)))
        except Exception as e:
            # died so remove self and any other dead from path
            print("disconnected {0} from {1}".format(str(websocket.remote_address), path))
            DB[path] = [x for x in DB[path] if not x.closed]
            break

start_server = websockets.serve(newconnection, config.WS_SERVER_HOST, config.WS_SERVER_PORT)

asyncio.get_event_loop().run_until_complete(start_server)

print("start")
try:
    asyncio.get_event_loop().run_forever()
except KeyboardInterrupt:
    print("stopped by user - KeyboardInterrupt")
except Exception as e:
    print("error while running - {}".format(repr(e)))

print("stop")
