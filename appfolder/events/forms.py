from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, DateTimeField
from wtforms.validators import InputRequired, Optional, Length


class CreateEventForm(FlaskForm):
    title = StringField("title", validators=[InputRequired(), Length(min=8, max=128)])
    message = StringField("message", validators=[InputRequired(), Length(min=8, max=512)])
    event_type_id = IntegerField("event_type_id", validators=[InputRequired()])
    start = DateTimeField("start", validators=[Optional()], format='%Y-%m-%d %H:%M:%S')
    stop = DateTimeField("stop", validators=[Optional()], format='%Y-%m-%d %H:%M:%S')

    def get_data(self):
        return [self.title.data, self.message.data, self.event_type_id.data, self.start.data, self.stop.data]


class UpdateEventForm(CreateEventForm):
    id = IntegerField("id", validators=[InputRequired()])

    def get_data(self):
        return [self.id.data] + super().get_data()
