from flask import request
from flask_restful import Resource
from sqlalchemy import or_
from dateutil.parser import parse

from .models import Events, EventTypes, get_event, delete_event, create_event, update_event
from .forms import CreateEventForm, UpdateEventForm
from ..api_errors import EventNotFound, InvalidEvent, EventFailed
from ..websocket_utils import WebSocket
from .. import app


def websocket_event(func):
    def run(*args, **kwargs):
        result = func(*args, **kwargs)

        if app.config["WS_SERVER_ENABLED"]:
            """
                if event is created/updated/deleted
                send a signal to the websocket server to trigger subscribers to update their data
                I am using this for real time data (of course there are other ways)
            """

            ws = WebSocket(app.config["WS_SERVER_HOST"], app.config["WS_SERVER_PORT"])
            ws.connect()
            ws.send("new")
            ws.close()

        return result
    return run

class EventTypesResource(Resource):

    def get(self):
        # get all event types
        return {
            "status": 200,
            "result": [eventtype.to_dict() for eventtype in EventTypes.query.all()]
        }

class EventsResource(Resource):

    def get(self):
        # get all events
        return {
            "status": 200,
            "result": [event.to_dict() for event in Events.query.all()]
        }

    def post(self):
        # get events filtered
        search = request.form.get("search")
        dates = {}
        for key in ["created_from", "created_to", "start_from", "start_to", "stop_from", "stop_to"]:
            try:
                dates[key] = parse(request.form.get(key))
            except:
                dates[key] = None
        try:
            event_type_id = int(request.form.get("event_type"))
        except:
            event_type_id = None
        try:
            page = int(request.form.get("page"))
        except:
            page = 1
        try:
            page_length = int(request.form.get("page_length"))
        except:
            page_length = 50

        query = Events.query.join(EventTypes)

        total = query.count()

        # filter by search
        if search is None or search == "":
            filtered = total
        else:
            query = query.filter(
                or_(
                    Events.title.op('~')('(?i)' + search),
                    EventTypes.description.op('~')('(?i)' + search)
                )
            )
            filtered = query.count()

        # filter by event type id
        if event_type_id is not None:
            query = query.filter(Events.event_type_id == event_type_id)
        # filter by creation date
        if dates["created_from"] is not None:
            query = query.filter(Events.created_at >= dates["created_from"])
        if dates["created_to"] is not None:
            query = query.filter(Events.created_at <= dates["created_to"])
        # filter by start date
        if dates["start_from"] is not None:
            query = query.filter(Events.start >= dates["start_from"])
        if dates["start_to"] is not None:
            query = query.filter(Events.start <= dates["start_to"])
        # filter by stop date
        if dates["stop_from"] is not None:
            query = query.filter(Events.stop >= dates["stop_from"])
        if dates["stop_to"] is not None:
            query = query.filter(Events.stop <= dates["stop_to"])

        # apply page limits
        res = query.limit(
            page_length
        ).offset(
            (page - 1) * page_length
        ).all()

        result = [event.to_dict() for event in res]

        return {
            "status": 200,
            "result": {
                "data": result,
                "total_records": total,
                "filtered_records": filtered,
                "page": page,
                "page_length": page_length
            }
        }

class EventResource(Resource):

    def get(self, event_id):
        # read event
        event = get_event(event_id)

        if event is not None:
            return {
                "status": 200,
                "result": event.to_dict()
            }

        raise EventNotFound

    @websocket_event
    def put(self, event_id):
        # update event
        event = get_event(event_id)

        if event is not None:

            form = UpdateEventForm(request.form)

            if form.validate():
                updated_event = update_event(*form.get_data())

                if updated_event is not None:

                    return {
                        "status": 200,
                        "result": updated_event.to_dict()
                    }

                raise EventFailed

            raise InvalidEvent

        raise EventNotFound

    @websocket_event
    def delete(self, event_id):
        # delete event
        if delete_event(event_id):
            return {
                "status": 200
            }

        raise EventNotFound

class EventResource2(Resource):

    @websocket_event
    def post(self):
        # create event

        form = CreateEventForm(request.form)

        if form.validate():
            event = create_event(*form.get_data())

            if event is not None:

                return {
                    "status": 200,
                    "result": event.to_dict()
                }

            raise EventFailed

        raise InvalidEvent


def add_resource(api, apiurl):

    api.add_resource(EventTypesResource, apiurl + "eventtypes/")
    api.add_resource(EventsResource, apiurl + "events/")
    api.add_resource(EventResource, apiurl + "event/<int:event_id>/")
    api.add_resource(EventResource2, apiurl + "event/")
