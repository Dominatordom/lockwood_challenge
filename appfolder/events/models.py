from .. import db, app
from dateutil.parser import parse
from datetime import datetime

db.Model.metadata.reflect(db.engine, schema='public')

class Events(db.Model):

    __tablename__ = 'public.events'
    __table__ = db.Model.metadata.tables[__tablename__]
    event_type = db.relationship("EventTypes", backref="events")

    def to_dict(self):
        return {
            "id": self.id,
            "event_type": self.event_type.to_dict(),
            "created_at": str(self.created_at),
            "start": None if self.start is None else str(self.start),
            "stop": None if self.stop is None else str(self.stop),
            "title": self.title,
            "message": self.message
        }

    def set_attributes(self, title, message, event_type_id, start, stop, change_created=True):
        self.title = title
        self.message = message
        self.event_type_id = event_type_id
        if start is not None:
            self.start = parse(start)
        else:
            self.start = None
        if stop is not None:
            self.stop = parse(stop)
        else:
            self.stop = None
        if change_created:
            self.created_at = datetime.utcnow()


class EventTypes(db.Model):

    __tablename__ = 'public.event_types'
    __table__ = db.Model.metadata.tables[__tablename__]

    def to_dict(self):
        return {
            "id": self.id,
            "description": self.description
        }


def _commit(event):
    try:
        db.session.add(event)
        db.session.commit()
    except Exception as e:
        app.logger.error("commit_event - %s" % repr(e))
        db.session.rollback()
        return None
    return event


def get_events():
    return Events.query.all()


def get_event(event_id):
    event = Events.query.get(event_id)
    if event is None:
        app.logger.error("get_event, could not find event with id {}".format(event_id))
    return event


def create_event(title, message, event_type_id, start=None, stop=None):
    event = Events()
    event.set_attributes(title, message, event_type_id, start=None, stop=None)
    return _commit(event)


def update_event(event_id, title, message, event_type_id, start=None, stop=None):
    event = get_event(event_id)

    if event is None:
        return None

    event.set_attributes(title, message, event_type_id, start=None, stop=None, change_created=False)
    return _commit(event)


def delete_event(event_id):
    event = Events.query.get(event_id)

    if event is None:
        return False

    try:
        db.session.delete(event)
        db.session.commit()
    except Exception as e:
        app.logger.error("delete_event - %s" % repr(e))
        db.session.rollback()
        return False
    return True
