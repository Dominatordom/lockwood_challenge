from flask import request, jsonify
from sqlalchemy import or_

from .. import app
from ..controllers import render_template
from .models import Events, EventTypes


class events:
    def list(self):

        # Prepare template variables
        template_vars = {
            "ws_server": app.config["WS_SERVER_ENABLED"],
            "ws_server_link": "{0}:{1}".format(app.config['WS_SERVER_HOST'], app.config["WS_SERVER_PORT"]),
            "menuname": "list_events"
        }

        return render_template("events/list.html", data=template_vars)

    def gettable(self):
        # function to populate datatables in list

        fields = ["id", "title", "message", "event_type_id", "start", "stop", "created_at"]

        query = Events.query.join(EventTypes)

        total = query.count()

        search = request.args.get("search")

        if search == "":
            filtered = total
        else:
            query = query.filter(
                or_(
                    Events.title.op('~')('(?i)' + search),
                    EventTypes.description.op('~')('(?i)' + search)
                )
            )
            filtered = query.count()

        order_by = int(request.args.get("order"))

        if order_by == 3:
            query = query.order_by(
                getattr(EventTypes.description, request.args.get("orderdir"))()
            )
        else:
            query = query.order_by(
                getattr(getattr(Events, fields[order_by]), request.args.get("orderdir"))()
            )

        res = query.limit(
            int(request.args.get("length"))
        ).offset(
            int(request.args.get("start"))
        ).all()

        result = [{
            "id": event.id,
            "title": event.title,
            "message": event.message,
            "event_type": event.event_type.description,
            "start": "" if event.start is None else str(event.start),
            "stop": "" if event.stop is None else str(event.stop),
            "created_at": str(event.created_at)
        } for event in res]

        return jsonify({"data": result, "draw": int(request.args.get("draw")), "recordsTotal": total, "recordsFiltered": filtered})

