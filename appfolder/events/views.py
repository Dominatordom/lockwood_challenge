from flask import Blueprint
from .controllers import events


class events_module(events):
    def __init__(self):
        # Define the blueprint: 'events', set its url prefix: app.url/events
        # Both static and templates folder are set to local dir
        self.blueprint = Blueprint(
            'events',
            __name__,
            url_prefix='/events',
            static_folder='static',
            template_folder='./templates'
        )

    def get_blueprint(self):
        return self.blueprint

    def create_routes(self):
        @self.blueprint.route("/", methods=['GET'])
        def list():
            return self.list()

        @self.blueprint.route("/gettable/", methods=['GET'])
        def gettable():
            return self.gettable()
