const ws_server = $("#ws_server").val() == "True"
const ws_server_link = $("#ws_server_link").val()
var table = undefined;
var websocket_object;

function create_datatable() {
    table = $('#event-table').DataTable({
        "responseive": true,
        "deferRender": true,
        "serverSide": true,
        "serverMethod": "GET",
        "ajax": {
            "url": "/events/gettable/",
            "data": function(d) {
                return {
                    "draw": d.draw,
                    "start": d.start,
                    "length": d.length,
                    "order": d.order[0].column,
                    "orderdir": d.order[0].dir,
                    "search": d.search.value
                };
            }
        },
        "columns": [
            { "data": "id", "title": "Event ID" },
            { "data": "title", "title": "Title" },
            { "data": "message", "title": "Message" },
            { "data": "event_type", "title": "Event Type" },
            { "data": "start", "title": "Event Start" },
            { "data": "stop", "title": "Event Stop" },
            { "data": "created_at", "title": "Created" }
        ],
        "destroy": true,
        "stateSave": false,
        "bFilter": true,
        "bInfo": true,
        "paging": true,
        "pageLength": 100,
        "ordering": true,
        dom: 'Bfrtip',
        "columnDefs": [],
        "orderMulti": false,
        "searchDelay": 1000,
        "order": [
            [1, "asc"]
        ],
        "initComplete": function(settings, json) {},
        "drawCallback": function(settings, json) {}
    });

    $.fn.dataTable.ext.errMode = 'none';
}

function ws_new_event(event) {
    //when receiving a new event, refresh the table to get the new event
    table.search(table.search()).draw()
}

function create_websockets() {
    websocket_object = new WebSocket("ws://" + ws_server_link + "/receive");
    websocket_object.onmessage = ws_new_event
}

function main() {
    create_datatable();
    if (ws_server){
        create_websockets();
    }
}

$(document).ready(main);
