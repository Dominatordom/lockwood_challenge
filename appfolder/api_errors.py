from flask_restful import HTTPException

# this is used to map custom exception messages and status codes that can be used by the entire app
errors_dict = {
    "EventNotFound": {
        "message": "The requested event could not be found.",
        "status": 404
    },
    "InvalidEvent": {
        "message": "Invalid attributes for event.",
        "status": 400
    },
    "EventFailed": {
        "message": "Create/update event failed.",
        "status": 400
    }
}


class EventNotFound(HTTPException):
    # exception for requests for event that doesn't exist
    pass


class InvalidEvent(HTTPException):
    # exception for invalid attributes submitted for an event
    pass


class EventFailed(HTTPException):
    # exception for errors when creating or updating an event
    pass
