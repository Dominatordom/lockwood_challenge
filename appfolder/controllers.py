from flask import render_template as flask_render
from . import app


@app.route('/', methods=['GET'])
def homepage():

    # Prepare template variables
    template_vars = {
        'menuname': "homepage"
    }

    return render_template('homepage.html', data=template_vars)


def render_template(url, **kwargs):

    kwargs["sys"] = {
        "version": app.config['APP_VERSION'],
        "appname": app.config['APP_NAME'],
        # "wsserver": "%s:%s" % (app.config['WSSERVER_HOST'], app.config["WSSERVER_PORT"])
    }

    if "data" not in kwargs:
        kwargs["data"] = {}

    return flask_render(url, **kwargs)
