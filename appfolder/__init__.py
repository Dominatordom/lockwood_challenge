# Import flask and template operators
from flask import Flask

# Import flaks restfull
from flask_restful import Api
from .api_errors import errors_dict

# Import SQLAlchemy
# from flask.ext.sqlalchemy import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy

# Define the application object
app = Flask(__name__, static_url_path="/static")

# Configurations
app.config.from_object('config')

# Define the database object
db = SQLAlchemy(app)

# create api
api = Api(app, errors=errors_dict)
apiurl = "/api/"


# Import a modules / componenst using its blueprint handler
def register_blueprints(app):
    from .events.views import events_module
    events = events_module()
    events.create_routes()
    app.register_blueprint(events.get_blueprint())


# adding resources to the api
def add_resources(api):
    from .events.api import add_resource as events_api
    events_api(api, apiurl)


register_blueprints(app)
add_resources(api)

from .controllers import *
