from websocket import create_connection


class WebSocket():
    def __init__(self, host, port):
        self._host = host
        self._port = port
        self._ws = None

    def connect(self):
        if self._ws is not None:
            self.close()

        self._ws = create_connection("ws://{0}:{1}/send".format(self._host, self._port), timeout=None)

    def close(self):
        try:
            self._ws.close()
        except:
            pass

    def send(self, payload):
        try:
            self._ws.send(payload)
            self._ws.recv()
            return True
        except:
            return False
