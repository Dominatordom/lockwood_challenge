import os

# Import configparser to avoid version control of some values
from configparser import ConfigParser

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Define the cookie name based on folder definition
SESSION_COOKIE_NAME = BASE_DIR.split("/")[-1]

# define app configuration
APP_CONFIG = "config.cfg"
APP_NAME = "lockwood challenge"
APP_VERSION = "1.0"

# turn off modification tracking to save on overhead
SQLALCHEMY_TRACK_MODIFICATIONS = False

# turn off csrf token validation for forms
WTF_CSRF_ENABLED = False

config_file = ConfigParser()
try:
    config_file.read(APP_CONFIG)
except Exception as ex:
    print(repr(ex))
else:
    # define more app configuration
    DEBUG = config_file.getboolean("main", "debug")
    APP_ADDRESS = config_file.get("main", "address")
    APP_PORT = config_file.getint("main", "port")

    # Define database configuration
    DB_HOST = config_file.get("database", "host")
    DB_USER = config_file.get("database", "user")
    DB_PW = config_file.get("database", "password")
    DB_NAME = config_file.get("database", "name")
    DB_PORT = config_file.getint("database", "port")
    DB_DRIVER = config_file.get("database", "driver")

    # define websocket server configuration
    WS_SERVER_ENABLED = config_file.getboolean("wsserver", "enabled")
    WS_SERVER_HOST = config_file.get("wsserver", "host")
    WS_SERVER_PORT = config_file.get("wsserver", "port")


SQLALCHEMY_DATABASE_URI = "{driver}://{user}:{pw}@{host}:{port}/{db}".format(driver=DB_DRIVER, user=DB_USER, pw=DB_PW, host=DB_HOST, db=DB_NAME, port=str(DB_PORT))
DATABASE_CONNECT_OPTIONS = {}
HOST_URL = "%s:%s" % (APP_ADDRESS, APP_PORT)
