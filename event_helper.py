import requests
import config
from dateutil.parser import parse

TITLE_MIN = 8
TITLE_MAX = 128
MESSAGE_MIN = 8
MESSAGE_MAX = 512

while True:
    try:
        event_id = int(input("event id (0 = new): "))
    except:
        pass
    else:
        break

while True:
    title = input("title (length {min} to {max}): ".format(min=TITLE_MIN, max=TITLE_MAX))
    if len(title) >= TITLE_MIN and len(title) <= TITLE_MAX:
        break

while True:
    message = input("message (length {min} to {max}): ".format(min=MESSAGE_MIN, max=MESSAGE_MAX))
    if len(message) >= MESSAGE_MIN and len(message) <= MESSAGE_MAX:
        break

while True:
    try:
        event_type_id = int(input("event type (int): "))
    except:
        pass
    else:
        break

while True:
    start = input("event start (YYYY-MM-DD HH:mm:ss) (optional): ")
    if start == "":
        break
    try:
        start = str(parse(start))
    except:
        pass
    else:
        break

while True:
    stop = input("event stop (YYYY-MM-DD HH:mm:ss) (optional): ")
    if stop == "":
        break
    try:
        stop = str(parse(stop))
    except:
        pass
    else:
        break

payload = {
    "title": title,
    "message": message,
    "event_type_id": event_type_id
}

if start != "":
    payload["start"] = start

if stop != "":
    payload["stop"] = start

if event_id != 0:
    payload["id"] = event_id

url = "http://{url}/api/event/{event_id}".format(url=config.HOST_URL, event_id="" if event_id == 0 else "{}/".format(event_id))
request_type = "post" if event_id == 0 else "put"
response = getattr(requests, request_type)(url, data=payload)

if response.status_code != 200:
    print("recived {0} - {1}".format(response.status_code, response.json()["message"]))
else:
    print("event {}".format("created" if event_id == 0 else "updated"))
