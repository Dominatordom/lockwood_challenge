-- data for event types

INSERT INTO event_types(description) values("new release");
INSERT INTO event_types(description) values("configuration change");
INSERT INTO event_types(description) values("crash or downtime");
INSERT INTO event_types(description) values("scheduled maintenance");
INSERT INTO event_types(description) values("version update");
INSERT INTO event_types(description) values("service disabled");
