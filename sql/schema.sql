-- base tables for events

CREATE TABLE event_types(
    id serial,
    description varchar(128) not null,
    Primary Key(id)
);

CREATE TABLE events(
    id serial,
    created_at timestamp not null,
    title varchar(128) not null,
    message varchar(512) not null,
    start timestamp,
    stop timestamp,
    event_type_id int not null,
    Primary Key(id),
    Foreign Key(event_type_id) references event_types(id)
);

