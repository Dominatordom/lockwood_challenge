# Read Me

This project was created to be submitted to Lockwood Publishing as a back-end challenge.

## Requirements

To install all requirements simply run the following command in the project root directory:

	pip install -r requirements.txt

## Configurations

The config.cfg file must be filled in with the correct values.

The main section contains configurations for the web server, these can be left as is.

The wsserver section contains configurations for the web socket server, these can be left as is.

The database section contains configurations for the location of the database to be used by the web server, this database must exist and contain the expected tables and relationships.

## Database

I used a PostgreSQL and the configurations reflect this, to also use a PostgreSQL database first create the database, then use the files in the sql folder to create the required tables and some initial data.

the sql files and config.cfg file should be edited in order to use other types of database.

## Web Server

To start the web server simply run:
	
	python3 run.py

### Pages

The sever contains two simple pages, a homepage with some information about the challenge and a events page to show the events currently in the database.

The base of the web page is a stripped down version of adminlte that is based on bootstrap.

The events list uses bootstrap datatables with server side processing, the search bar filters by the event's title and the event type's description (case insensitive), all fields can be ordered.

### API

The api can be accessed via the url /api and has the following routes:

	GET /eventtypes/ -> gets a list of all event types
	GET /events/ -> gets a list of all events
	POST /events/ -> gets a list of events applying some filters, see Event Filters section
	GET /event/<event id>/ -> gets an event by its id
	PUT /event/<event id>/ -> update an event by its id, see Event Fields section
	DELETE /event/<event id>/ -> delete an event by its id
	POST /event/ -> creates an new event, see Event Fields section
	
### Event Fields

To create or update an event the following fields are required

* title - string of length between 8 and 128
* message - string of length between 8 and 512
* start - timestamp, optional field
* stop - timestamp, optional field
* event_type_id - integer, event type must exist

### Event Filters

Use the following keys to filter events:

* page - (integer) the requested page
* page_length - (integer) the amount of events per page
* search - (string) searches the event's title and the event type's description, case insensitive
* event_type - (integer) filters by event type id
* created_from - (date string) created_at must be after this
* created_to  - (date string) created_at must be before this
* start_from - (date string) start must be after this
* start_to  - (date string) start must be before this
* stop_from - (date string) stop must be after this
* stop_to  - (date string) stop must be before this

## Web Socket Server

This server is required to allow for real time data in the events page, to use this set the config.cfg wsserver enabled value to True and run the following command:

	python3 websocket_server.py

When visiting the events page a web socket is opened automatically to connect to this server and when an event is created/updated/deleted a signal is sent to the connected web sockets and they will all update their data by requesting the selected page again to the server.

## Event Helper

Please use the event_helper.py script to create or update events, this is just a feature used for testing, run the following command:

	python3 event_helper.py

and follow the on screen instructions